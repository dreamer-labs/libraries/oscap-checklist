---
stages:
  - lint
  - test
  - build
  - validate

include:
  - project: dreamer-labs/repoman/gitlab-ci-templates
    file: /.gitlab-ci-commitchecker.yml
    ref: master

flake8:
  stage: lint
  image:
    name: bitnami/python:3
    entrypoint: [""]
  script:
    - pip install -U pip "flake8>=3.0.0,<4.0.0"
    - pip list --outdated | grep flake8 1>&2 || true
    - flake8 ansible_collections/dingus9/oscap_checklist
  only:
    - master
    - merge_requests

.test_module_raw_template: &test_module_raw_template
  coverage: '/total_coverage: ([0-9]{1,3})/'
  before_script:
    - apk add grep
    - |
      (cd ansible_collections/dingus9/oscap_checklist;
       pip install -r requirements.txt -r test-requirements.txt)
  script:
    - |
      (cd ansible_collections/dingus9/oscap_checklist;
       pytest --cov plugins/modules tests/unit)
    - "echo total_coverage: $(coverage report | tail -n1 | grep -Po '[0-9]{1,3}(?=%)' )"
  only:
    - master
    - merge_requests

test_ansible_2_10:
  stage: test
  image: dingus9/ansible-ci-pipeline:version-2.10.3
  <<: *test_module_raw_template

test_ansible_2_9:
  stage: test
  image: dingus9/ansible-ci-pipeline:version-2.9.15
  <<: *test_module_raw_template

test_python27_module_only:
  stage: test
  image: python:2.7-alpine
  variables:
    ANSIBLE_VERSION: 2.9.15
  <<: *test_module_raw_template
  before_script:
    - apk add grep alpine-sdk libffi-dev openssl-dev
    - pip install ansible==$ANSIBLE_VERSION
    - (cd ansible_collections/dingus9/oscap_checklist;
       pip install -r requirements.txt -r test-requirements.txt)

.build_collection_template: &build_collection_template
  script:
    - cd ansible_collections/dingus9/oscap_checklist
    - ansible-galaxy collection build
  artifacts:
    paths:
      - ansible_collections/dingus9/oscap_checklist/dingus9-oscap_checklist-*.tar.gz
  only:
    - master
    - merge_requests

build_collection_2_10:
  stage: build
  image: dingus9/ansible-ci-pipeline:version-2.10.3
  needs: ["test_ansible_2_10"]
  <<: *build_collection_template

build_collection_2_9:
  stage: build
  image: dingus9/ansible-ci-pipeline:version-2.9.15
  needs: ["test_ansible_2_9"]
  <<: *build_collection_template

.install_collection_template: &install_module_template
    before_script:
      - pip install dictdiffer
    script:
      - cd ansible_collections/dingus9/oscap_checklist
      - ansible-galaxy collection install dingus9-oscap_checklist-*.tar.gz
      - ansible-doc dingus9.oscap_checklist.oscap_checklist
      - ansible-playbook tests/ansible/test.yml -i tests/ansible/inventory.ini -e repo_path=$CI_PROJECT_DIR
    only:
      - master
      - merge_requests

install_collection_template_2_10:
  stage: validate
  image: dingus9/ansible-ci-pipeline:version-2.10.3
  needs: ["build_collection_2_10"]
  <<: *install_module_template
  dependencies:
    - build_collection_2_10

install_collection_template_2_9:
  stage: validate
  image: dingus9/ansible-ci-pipeline:version-2.9.15
  needs: ["build_collection_2_9"]
  <<: *install_module_template
  dependencies:
    - build_collection_2_9
