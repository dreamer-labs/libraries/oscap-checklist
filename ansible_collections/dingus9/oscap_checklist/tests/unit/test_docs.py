import os
import subprocess

pth = os.path.realpath(os.path.join(__file__, '..', '..', '..', 'plugins', 'modules'))


def test_ansible_docs():

    docs = subprocess.check_output(['ansible-doc', '-M', pth, 'oscap_checklist'])

    assert 'OSCAP_CHECKLIST' in str(docs)
